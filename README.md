# Open PGP Signature Verification (OPSV)

[OPSV](https://opsv.foss.guru) is a FOSS solution for easy PGP signature verification.

## WARNING

OPSV is integrated in the larger keyoxide project and will therefore no longer be updated.

https://codeberg.org/yarmo/keyoxide

## About

This project uses [openpgp.js](https://openpgpjs.org/) loaded in the browser, meaning all processing is done on the device itself and no data is ever sent to the server. It supports loading public keys directly through:
1. plaintext input
2. web key directory (WKD)
3. HTTP Keyserver Protocol (HKP).

OPSV will always use the first input method it detects in the order described above.

It's also possible to not provide a public key. Read more about this in the `Using no public key at all` section below.

## Usage

Visit https://opsv.foss.guru/. On this website, you can enter a signed message (see example below) and any of the three supported public key inputs to verify that the owner of that public key was indeed the person to have signed that message.

## Example

Let's say I, Yarmo, would really like the world to know that I like pineapple. Using my private key, I've signed that statement so you can verify I wrote that message.

The signed statement:

```plaintext
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I like pineapple.
-----BEGIN PGP SIGNATURE-----

iQJDBAEBCAAtFiEEog/Pt4tEmnyVrrtlNzZ/SvQIetEFAl70mVUPHHlhcm1vQHlh
cm1vLmV1AAoJEDc2f0r0CHrRQXIP/08uza9zOtmZXv5K+uPGVzDKwkgPgZJEezX7
6iQ358f1pjSRvYfQ5aB13k2epUHoqCKArMYu1zPqxhvLvvAvp8uOHABnr9NGL3El
u7UUgaeUNHkr0gxCKEq3p81abrrbbWveP8OBP4RyxmaFx13Xcj7mfDluiBHmjVvv
WU09EdH9VPlJ7WfZ+2G2ZZDHuE5XiaeP7ocugTxXXLkp33zwpDX0+ZuCIXM6fQGe
OccSffglFPdNBnfasuuxDWxTQPsEbWGOPJV+CAPmBDeApX+TBF9bovO3hw4Uozk2
VT7EAy8Hb0SOrUb3UNGxzoKv++5676IxyB4JXX0Tr9O4ZxhO8o9pEEHwirtn/J1+
MWven4gVlWM/6bMeUqx6ydyNc2nqF5059yfRmwGMlp09x82G4x1bcf6aDZ+5njDG
fS5T2OpXRIkZHJx8BhmZjsxiDR0KV44zwHpt06+96ef3EDWB0BcP6M+a5Rtc33zf
irRmQd2M6RLyXCYtdGIiiAFRuomw802U4F0P4LwVrZdbGA6ObqBv1k8BUFCMbMz8
Ab4hF7kO4z0Vh3JaKzcHey0pOzdNCPpAHZ51sAoAnFDM4PdMBgQxxVweCMu4KYMZ
FN8sNn42oY/b7gDmwCelVhgD+rvUn/a8+B7CDmCp+wIquyrjrTt00voATcb+ZPMJ
pTXJ/NcM
=rqTX
```

Use this as "Signature" on [OPSV](https://opsv.foss.guru/).

### Using plaintext public key

Now, let's check the signature. Go to [my personal website](https://yarmo.eu/pgp) and copy-paste the "plaintext" key in the "Public Key (1: plaintext)" field.

You will see a green message confirming that my key was used to sign this message. I really do like pineapple.

### Using web key directory (WKD)

Remove the contents from the "Public Key (1: plaintext)" field. Now, in the "Public Key (2: web key directory)", write `yarmo@yarmo.eu` and verify the signature again. It is still verified. Try using `jane@doe.org` or any other input, it won't verify.

### Using HTTP Keyserver Protocol (HKP)

Remove the contents from the "Public Key (2: web key directory)" field. I uploaded my keys to the https://keys.openpgp.org/ HKP server, which is the default server used by OPSV. All you need to do is once again go to [my personal website](https://yarmo.eu/pgp) and copy-paste the "Fingerprint" in the "Public Key (3: HKP)" field (the second field!). Still verified!

### Using no public key at all

Wait, what? Then what am I verifying the signature against?

PGP signatures can contain the `userId` of the signer. If OPSV finds a `userId`, it will use it to perform a HKP lookup.

Remove the contents from the "Public Key (3: HKP)" field. It again verifies BUT against the information contained within the signature itself. You should carefully check the information OPSV returns. In this case, the authenticity is confirmed because the `userId` (yarmo@yarmo.eu) matches the one I use.

The signature below does not contain a `userId`:

```plaintext
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I like pineapple.
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEog/Pt4tEmnyVrrtlNzZ/SvQIetEFAl70mT4ACgkQNzZ/SvQI
etHUNBAAlswF4Q5IkPTsMELZPNHBps8CUJUeDWu3HlSz7c2U+4h2jJztHD0mDtxH
PqKzUnqQqNF1Bot//5xoOcn+m6UaSCzDk1oQFwD6LlQA+ScnIXddoV3xLqzTRAMe
dyuqOoDzoVeD+fWlwisnGElYX5jHRX6tgyKNh0auR3/crQUIJazAyeDwZFdJiwaL
ntd+d8T0BcVlVVPYN7RIp1hpT+PLIcwIsr64Myfy8SOa4cjVcQgnrhR/Lfz9680T
LCpnSohHRiA82nMGRiapEv+s+zy1NUZnVYbU2Li+Q0nYdSoDFu0xEBYmLOxwS50H
j6kK0ZyRicNeq2T25aIlieliTmSFLHHpzi/Zw8Yt1+FtZvWf4pstA19ahk7AQK5W
zYF2bMO2xn5D4/pRz1P4e2NTWYeIK+ZHttc7T9ZSS9Ffo03fjcJXhson3WcQZKB5
VIGVVFnlWujNYYotmxys84OtE6ePfVRwHasIOLfknVq64RVo68Y1Pgw/KPXSb1k6
3r+YD0mt5i/NWpwm79G/Aq54WI5JT905div88d0Bbpa3dScTZ2MiBJbP96pZBcKl
dpm3RnjsbCFgZqEpclrEh2SD1e8eCjrNcouWK3jIfOkaWB2xk1KvNmdyQQTs3dkP
/CpKcCJiNVvY9ogWxg9aUuQZUn4WvCvaEkmP4dfkk9s8yAKPQf8=
=QqCq
```

Once again, the signature verifies. And again, it only verifies against the information contained within itself so **that doesn't prove anything about its authenticity**. Anyone can write this and the signature will return verified.

 Except now, there is no `userId` for easy manual verification. So, you need to either take the `keyId` or the `fingerprint` and find some other way of verifying it, for example by contacting the person who supposedly wrote the message.

 In my case, you can simply visit [my personal website](https://yarmo.eu/pgp) and compare the `fingerprint`.

### What can a bad actor do?

One could not sign a statement with my private key: I, and only I, have access to it.

One could however simply take any of my signed messages and change the content. Like so:

```plaintext
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I like privacy invasion.
-----BEGIN PGP SIGNATURE-----

iQJDBAEBCAAtFiEEog/Pt4tEmnyVrrtlNzZ/SvQIetEFAl70la8PHHlhcm1vQHlh
cm1vLmV1AAoJEDc2f0r0CHrRDcYP/R4Yo+xiBLHtrOEMAQQkwbxWyQgCbjS4h9iF
As86o9a+t5dKSsL4gSoB3sdNAL0a1ZOhaAU8kWaR6xN1RvCQjFr878hEf631yai6
DfF2eRZPEsjXkAzlOKlPrAvtrNwUUMSDk20rGa4A9HHdxpfrmDRIgVaz3uNr1qqc
N/Ag3OK/2l1pZFTqjPekqDnXwblLiuTTLFlMlS80LFKoa7zZLkE5SD5O6WQFpOK7
DwYJk1+UjWDgVugz8rSLWag0mag9Z815furPIkU9yRmE1tIjsgpCR+uJA/e0I4bn
4Ei0M29df1QucDNv6q2WoW/7rCMz1IY796TY/BbdqbFk6vOUUHu596mQB+fJDNTX
jGC0SpJPhzhgoZICzK8yWJMJGoLXScYj95rCAqjYdnW/LDdAgODCyjSOxnzdI1zi
prQf4OmayHzDjI8Bo4bl22toPdSIDt3r5MmSGXcmBrNU16ea7FC9MnR8dkKfHD55
tC3UL2Ps/iU76kqzGAei1PKvaVqKogUGi/kqWzfi2eg+useHRyZpJrJv3R2mE0Y2
eSLMMJ5cTuM60c0GSIPOxzBBsMRwa0HmEQ3HKgpnpkVYxoA00/hq91kuNavqUqM+
OyOgbb21woPAG+S4OCHkOINEAooeCfhpSFtmpa87sUcfvDHUuX1ivL4rYoQO3cT2
gNfjdSiB
=tqZV
```

Given the wording of the statement, you naturally doubt the origin of it being me. You run it through OPSV and indeed, this is not what I wrote!

You know me, "I despise privacy invasion." (hint hint).
